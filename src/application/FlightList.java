package application;


public class FlightList {
	private Flight[] ac; //List of planes
	private  String lastDv; //Timestamp for further query
	
	public FlightList() {
		super();
	}

	 
	public Flight[] getAcList() {
		return ac;
	}

	public void setAcList(Flight[] acList) {
		this.ac = acList;
	}

	public String getLastDv() {
		return lastDv;
	}

	public void setLastDv(String lastDv) {
		this.lastDv = lastDv;
	}
	
	public int getSize() {
		return ac.length;
	}

	
	@Override
	public String toString() {
		return "FlightList [acList=" + ac + ", lastDv=" + lastDv + "]";
	}
}
